const path = require("path");
const http = require("http");
const http_status = require("http-status-codes");
const fs = require("fs");
const express = require("express");
const body_parser = require("body-parser");
const cookie_parser = require("cookie-parser");

const hostname = "127.0.0.1";
const port = 3000;

var app = express();
app.use(cookie_parser());
app.use(body_parser.urlencoded({ extended: false }));
app.use(body_parser.json());
var dir = path.join(__dirname, "public");
app.use(express.static(dir));

var board = Array(8).fill("empty").map(() => Array(8).fill("empty"));

for (var x = 0; x < 8; x++) {
    board[x][1] = "black_pawn";
}

board[0][0] = "black_rook";
board[1][0] = "black_knight";
board[2][0] = "black_bishop";
board[3][0] = "black_queen";
board[4][0] = "black_king";
board[5][0] = "black_bishop";
board[6][0] = "black_knight";
board[7][0] = "black_rook";

for (var x = 0; x < 8; x++) {
    board[x][6] = "white_pawn";
}

board[0][7] = "white_rook";
board[1][7] = "white_knight";
board[2][7] = "white_bishop";
board[3][7] = "white_queen";
board[4][7] = "white_king";
board[5][7] = "white_bishop";
board[6][7] = "white_knight";
board[7][7] = "white_rook";

// const WebSocket = require('ws');

// const server = new WebSocket.Server({
//     port: 3001
// });

// let sockets = [];
// server.on('connection', function(socket) {
//     sockets.push(socket);

//     socket.on('message', function(data) {
// 	console.log('received: %s', data);
//     });

//     socket.on('close', function() {
// 	sockets = sockets.filter(s => s !== socket);
//     });
// });


function username() { return (Math.random() + 1).toString(36).substring(7); }

app.get("/", (req, res) =>
{
    var options = {
        root: path.join(__dirname, "public")
    };
    console.log("Sending main html and login cookie");
    res.cookie("username", username());
    res.sendFile("chess.html", options);
});

app.get("/board", (req, res) =>
{
    console.log("Getting board");

});


// app.post("/send_message", (req, res) =>
// {
//     // let message = req.body["message"] + " from " + req.cookies["username"];
//     let message = req.cookies["username"] + ": " + req.body["message"];

//     content += message + "\n";

//     sockets[0].send(content);

//     // console.log("Sending message");
// });

// app.get("/get_messages", (req, res) =>
// {
//     // console.log("Getting messages " + content);
//     // res.send(content);
//     console.log("Gettings messages");
//     sockets[0].send(content);
// });

// app.get("/send_cookie", (req, res) =>
// {
//     console.log("Sending cookie..");

//     res.cookie("test cookie", "123");
//     res.send("Cookie sent.");
// });

// app.get("/get_cookie", (req, res) =>
// {
//     console.log("Getting cookie..");
//     console.log(req.cookies);
//     res.send(req.cookies);
// });

app.listen(port, () =>
{
    console.log("Example listening..");
});

