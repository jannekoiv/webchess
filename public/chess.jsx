

var root = document.getElementById("root");

var board = Array(8).fill("empty").map(() => Array(8).fill("empty"));

for (var x = 0; x < 8; x++) {
    board[x][1] = "black_pawn";
}

board[0][0] = "black_rook";
board[1][0] = "black_knight";
board[2][0] = "black_bishop";
board[3][0] = "black_queen";
board[4][0] = "black_king";
board[5][0] = "black_bishop";
board[6][0] = "black_knight";
board[7][0] = "black_rook";

for (var x = 0; x < 8; x++) {
    board[x][6] = "white_pawn";
}

board[0][7] = "white_rook";
board[1][7] = "white_knight";
board[2][7] = "white_bishop";
board[3][7] = "white_queen";
board[4][7] = "white_king";
board[5][7] = "white_bishop";
board[6][7] = "white_knight";
board[7][7] = "white_rook";

function square_highlight_color(color)
{
    if (color == "bisque") {
        return "lawngreen";
    } else {
        return "forestgreen";
    }
}

function square_regular_color(color)
{
    if (color == "lawngreen") {
        return "bisque";
    } else {
        return "peru";
    }
}


function id_from_position(x, y)
{
    return x.toString() + y.toString();
}

var selected_x = -1;
var selected_y = -1;

function swap_squares(board, x1, y1, x2, y2)
{
    var tmp = board[x1][y1];
    board[x1][y1] = board[x2][y2];
    board[x2][y2] = tmp;
}

function handle_click(x, y)
{
    var e = document.getElementById(id_from_position(x, y));

    var pos_x = x / e.clientWidth;
    var pos_y = y / e.clientHeight;

    if (selected_x < 0 && selected_y < 0 && board[pos_x][pos_y] != "empty") {
        e.style.backgroundColor = square_highlight_color(e.style.backgroundColor);
        selected_x = pos_x;
        selected_y = pos_y;
    } else {

        var id2 = id_from_position(selected_x * e.clientWidth, selected_y * e.clientHeight);
        var e2 = document.getElementById(id2);
        e2.style.backgroundColor = square_regular_color(e2.style.backgroundColor);

        swap_squares(board, selected_x, selected_y, pos_x, pos_y);

        selected_x = -1;
        selected_y = -1;

        draw_board();
    }
}

function image(props)
{
    if (props.file != "empty.svg") {
        return <img src={props.file} width="100px" height="100px" alt="PAWN" draggable="false" />;
    } else {
        return <div></div>;
    }
};

function Square(props)
{
    var x = props.pos_x;
    var y = props.pos_y;

    var id = id_from_position(x, y);
    return <div
        id={id}
        style=
        {{
            position: "absolute",
            left: x + "px",
            top: y + "px",
            width: "100px",
            height: "100px",
            background: props.color
        }}
        onClick={() => { handle_click(x, y); }} >
        {image(props)}
    </div>;
}

function square_initial_color(i)
{
    if (i % 2 == 0) {
        return "bisque";
    } else {
        return "peru";
    }
}

function draw_board()
{
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", "http://localhost:3000/board", true);

    xhttp.setRequestHeader("Content-Type", "application/json");
    // var data = JSON.stringify({ "message": msg });
    xhttp.send();

    var elements = [];
    var i = 0;

    for (var y = 0; y < 8; y++) {
        for (var x = 0; x < 8; x++) {
            elements.push(<Square pos_x={x * 100} pos_y={y * 100} color={square_initial_color(i)} file={board[x][y] + ".svg"} />);
            i++;
        }
        i++;
    }

    ReactDOM.render(
        elements,
        root
    );
};

draw_board();


